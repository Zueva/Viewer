package com.appviewer.appviewer.entity;

import java.math.BigDecimal;

/**
 * Created by Yulya on 24.03.2017.
 */

public interface Product {
    String getSku();

    String getCurrency();

    String getAmount();

    void setAmountConverted(BigDecimal amount);

    BigDecimal getAmountConverted();
}
