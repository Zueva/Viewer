package com.appviewer.appviewer.entity;

/**
 * Created by Yulya on 24.03.2017.
 */

public class Rate {
    private String from;
    private String rate;
    private String to;

    public Rate(String from, String rate) {
        this.from = from;
        this.rate = rate;
    }

    public Rate(String from, String rate, String to) {
        this(from, rate);
        this.to = to;
    }
    public String getFrom() {
        return from;
    }

    public String getRate() {
        return rate;
    }

    public String getTo() {
        return to;
    }
}
