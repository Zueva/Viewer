package com.appviewer.appviewer.entity;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Created by Julya on 10.04.2017.
 */

public class Transactions {
    BigDecimal total;
    Set<Product> transactions;

    public Transactions(Set<Product> transactions, BigDecimal total) {
        this.total = total;
        this.transactions = transactions;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void addCost(BigDecimal cost) {
        total = total.add(cost);
    }

    public Set<Product> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Product> transactions) {
        this.transactions = transactions;
    }
}
