package com.appviewer.appviewer.entity;

/**
 * Created by Julya on 10.04.2017.
 */

public class Products {
    String name;
    int count;

    public Products(String name, int count) {
        this.name = name;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
