package com.appviewer.appviewer.entity;

import java.math.BigDecimal;

/**
 * Created by Yulya on 24.03.2017.
 */

public class ProductImpl implements Product {
    private String amount;
    private String currency;
    private String sku;
    private BigDecimal amountConverted;

    @Override
    public String getSku() {
        return sku;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public String getAmount() {
        return amount;
    }

    @Override
    public void setAmountConverted(BigDecimal amaount) {
        amountConverted = amaount;
    }

    @Override
    public BigDecimal getAmountConverted() {
        return amountConverted;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 17;
        result = prime * result + amount.hashCode();
        result = prime * result + currency.hashCode();
        result = prime * result + sku.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Product)) {
            return false;
        }

        Product other = (Product) o;

        return  other.getAmount().equals(amount) &&
                other.getCurrency().equals(currency) &&
                other.getSku().equals(sku);
    }
}
