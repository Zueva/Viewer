package com.appviewer.appviewer.products;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;

import com.appviewer.appviewer.entity.Products;
import com.appviewer.appviewer.entity.Transactions;
import com.appviewer.appviewer.repository.ProductsRepository;
import com.appviewer.appviewer.resources.ProductsLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

/**
 * Created by Julya on 31.03.2017.
 */

public class ProductsPresenter implements ProductContract.Presenter, ProductsRepository.LoadDataCallback,
        LoaderManager.LoaderCallbacks<Map<String, Transactions>>{

    @Inject ProductsRepository mProductsRepository;

    public final static int PRODUCTS_LOADER = 1;

    private final ProductContract.View mProductsView;

    private final ProductsLoader mLoaderProducts;

    private final LoaderManager mLoaderManager;

    private Map<String, Transactions> mCurrentTransactions;

    public ProductsPresenter(@NonNull ProductsLoader loaderProducts, @NonNull LoaderManager mLoaderManager,
                             @NonNull ProductContract.View mProductsView) {
        this.mLoaderProducts = loaderProducts;
        this.mLoaderManager = mLoaderManager;
        this.mProductsView = mProductsView;
    }

    @Override
    public void start() {
        mLoaderManager.initLoader(PRODUCTS_LOADER, null, this);
    }

    @Override
    public void loadProducts() {
        mProductsView.setLoadingIndicator(true);
        if(mProductsRepository.cachedTransactionsAvailable()) {
            onDataLoaded(mProductsRepository.getTransactions());
        } else {
            onDataEmpty();
        }
    }

    @Override
    public void onDataLoaded(Map<String, Transactions> data) {
        List<Products> products = new ArrayList<>();
        Set<Map.Entry<String, Transactions>> set = data.entrySet();
        for (Map.Entry<String, Transactions> entry : set) {
            products.add(new Products(entry.getKey(), entry.getValue().getTransactions().size()));
        }
        mProductsView.setLoadingIndicator(false);
        mProductsView.showProducts(products);
    }

    @Override
    public void onDataEmpty() {
        mProductsView.setLoadingIndicator(false);
        mProductsView.showNoProducts();
    }

    @Override
    public void onDataNotAvailable() {
        mProductsView.setLoadingIndicator(false);
        mProductsView.showLoadingProductsError();
    }

    @Override
    public void onDataReset() {
        mProductsView.showProducts(null);
    }

    @Override
    public android.support.v4.content.Loader<Map<String, Transactions>> onCreateLoader(int id, Bundle args) {
        mProductsView.setLoadingIndicator(true);
        return mLoaderProducts;
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Map<String, Transactions>> loader, Map<String, Transactions> data) {
        mProductsView.setLoadingIndicator(true);
        mCurrentTransactions = data;
        if (mCurrentTransactions != null) {
            if (!mCurrentTransactions.isEmpty()) {
                onDataLoaded(mCurrentTransactions);
            } else {
                onDataEmpty();
            }
        } else {
            onDataNotAvailable();
        }
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Map<String, Transactions>> loader) {
        onDataReset();
    }
}
