package com.appviewer.appviewer.products;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.appviewer.appviewer.R;
import com.appviewer.appviewer.entity.Products;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yulya on 24.03.2017.
 */

public class ProductAdapter extends BaseAdapter {
    private List<Products> products = new ArrayList();
    private Context mContext;

    public ProductAdapter(Context context) {
        mContext = context;
    }

    public void setProducts(List<Products> products){
        this.products = products;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;
        Products transaction = (Products) getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_product, null);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)view.getTag();
        }
        viewHolder.sku.setText(transaction.getName());
        viewHolder.transactions.setText(String.format("%s products", Integer.toString(transaction.getCount())));
        return view;
    }

    class ViewHolder {
        View view;
        @BindView(R.id.tvSku)
        TextView sku;
        @BindView(R.id.tvTransactions)
        TextView transactions;
        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }
}
