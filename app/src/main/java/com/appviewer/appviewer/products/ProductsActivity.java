package com.appviewer.appviewer.products;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.appviewer.appviewer.R;
import com.appviewer.appviewer.ViewerApplication;
import com.appviewer.appviewer.entity.Products;
import com.appviewer.appviewer.other.Constants;
import com.appviewer.appviewer.repository.ProductsRepository;
import com.appviewer.appviewer.resources.ProductsLoader;
import com.appviewer.appviewer.transactions.TransactionActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Julya on 31.03.2017.
 */

public class ProductsActivity extends AppCompatActivity implements ProductContract.View, AdapterView.OnItemClickListener {

    @Inject
    ProductsRepository mProductsRepository;

    @BindView(R.id.toolbar_progress_bar)
    protected ProgressBar progressBar;
    @BindView(R.id.lv)
    protected ListView lvProducts;
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    private ProductsPresenter mProductsPresenter;
    private ProductAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_product);
        ButterKnife.bind(this);

        toolbar.setTitle(getString(R.string.products));
        setSupportActionBar(toolbar);

        ViewerApplication.getComponent().inject(this);

        ProductsLoader productsLoader = new ProductsLoader(getApplicationContext(), mProductsRepository);
        mProductsPresenter = new ProductsPresenter(productsLoader, getSupportLoaderManager(), this);

        mAdapter = new ProductAdapter(this);
        lvProducts.setAdapter(mAdapter);
        lvProducts.setOnItemClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mProductsPresenter.start();
    }

    @Override
    public void showProducts(List<Products> products) {
        mAdapter.setProducts(products);
    }

    @Override
    public void showNoProducts() {
        Toast.makeText(this, getString(R.string.empty_data), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        progressBar.setVisibility(active ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showLoadingProductsError() {
        Toast.makeText(this, getString(R.string.error_data), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Products transaction = (Products) mAdapter.getItem(position);
        String name = transaction.getName();
        Intent intent = new Intent(this, TransactionActivity.class);
        intent.putExtra(Constants.PRODUCT_NAME, name);
        startActivity(intent);
    }
}
