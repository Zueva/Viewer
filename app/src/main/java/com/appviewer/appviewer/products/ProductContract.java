package com.appviewer.appviewer.products;

/**
 * Created by Julya on 31.03.2017.
 */

import com.appviewer.appviewer.BasePresenter;
import com.appviewer.appviewer.entity.Products;

import java.util.List;

public interface ProductContract {

    interface View{

        void showProducts(List<Products> products);

        void showNoProducts();

        void setLoadingIndicator(boolean active);

        void showLoadingProductsError();

    }

    interface Presenter extends BasePresenter {

        void loadProducts();
    }
}
