package com.appviewer.appviewer.resources;

import android.content.Context;
import android.content.res.AssetManager;

import com.appviewer.appviewer.entity.Product;
import com.appviewer.appviewer.entity.ProductImpl;
import com.appviewer.appviewer.entity.Rate;
import com.appviewer.appviewer.other.Constants;
import com.appviewer.appviewer.parser.RateParser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Yulya on 24.03.2017.
 */

public class DataLoader implements Loader {

    private final Context context;

    @Inject
    public DataLoader(Context context) {
        this.context = context;
    }

    @Override
    public boolean isExist(String fileName) {
        List<String> fList = null;
        AssetManager assetManager = context.getAssets();
        try {
            fList = Arrays.asList(assetManager.list(""));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (fList == null) return false;
        return fList.contains(fileName);
    }

    @Override
    public List<Product> getProducts() {
        String content = getStringContent(Constants.TRANSACTIONS);
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return gson.fromJson(content,
                new TypeToken<List<ProductImpl>>() {
                }.getType());
    }

    @Override
    public List<Rate> getRates() {
        RateParser parser = new RateParser();
        List<Rate> rates = new ArrayList<>();
        try {
            InputStream stream = context.getAssets().open(Constants.RATES);
            rates = parser.readJsonStream(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rates;
    }

    private String getStringContent(String fileName) {
        StringBuilder builder = new StringBuilder();
        try {
            InputStream stream = context.getAssets().open(fileName);
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
            String str;
            while ((str = reader.readLine()) != null) {
                builder.append(str);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return builder.toString();
    }
}
