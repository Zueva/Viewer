package com.appviewer.appviewer.resources;

import com.appviewer.appviewer.entity.Product;
import com.appviewer.appviewer.entity.Rate;

import java.util.List;

/**
 * Created by Yulya on 24.03.2017.
 */

public interface Loader {
    boolean isExist(String fileName);

    List<Product> getProducts();

    List<Rate> getRates();
}
