package com.appviewer.appviewer.resources;

import android.support.annotation.NonNull;
import android.support.v4.content.AsyncTaskLoader;
import android.content.Context;

import com.appviewer.appviewer.entity.Product;
import com.appviewer.appviewer.entity.Rate;
import com.appviewer.appviewer.entity.Transactions;
import com.appviewer.appviewer.math.Graph;
import com.appviewer.appviewer.other.Constants;
import com.appviewer.appviewer.repository.ProductsRepository;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Julya on 31.03.2017.
 */

public class ProductsLoader extends AsyncTaskLoader<Map<String, Transactions>> {
    private final ProductsRepository mRepository;

    public ProductsLoader(Context context, @NonNull ProductsRepository repository) {
        super(context);
        this.mRepository = repository;
    }

    @Override
    public Map<String, Transactions>  loadInBackground() {
        List<Product> products = mRepository.getProducts();
        List<Rate> rates = mRepository.getRates();
        Graph graph = new Graph();
        buildGraph(graph, rates);

        Map<String, Transactions> productsMap = new HashMap<>();
        for (Product product : products) {
            String name = product.getSku();
            BigDecimal multiplier = graph.getShortPath(new Graph.Vertex(product.getCurrency()));

            BigDecimal amount = new BigDecimal(product.getAmount());
            BigDecimal amountConverted = amount.multiply(multiplier);
            product.setAmountConverted(amountConverted);

            if (productsMap.containsKey(name)) {
                Transactions transactions = productsMap.get(name);
                Set<Product> productsSet = transactions.getTransactions();
                productsSet.add(product);
                transactions.addCost(amountConverted);
            }else {
                Set productsBySku = new HashSet();
                productsBySku.add(product);
                Transactions transactions = new Transactions(productsBySku, new BigDecimal("0.0"));
                productsMap.put(name, transactions);
            }
        }
        mRepository.saveTransactions(productsMap);
        return productsMap;
    }

    private void buildGraph(Graph graph, List<Rate> rates) {
        for (Rate rate : rates) {
            graph.addEdge(new Graph.Vertex(rate.getFrom()),
                    new Graph.Edge(new Graph.Vertex(rate.getFrom()),
                    new Graph.Vertex(rate.getTo()), rate.getRate()));
        }
        graph.execute(new Graph.Vertex(Constants.GBP));
    }

    @Override
    public void deliverResult(Map<String, Transactions>  data) {
        if (isReset()) {
            return;
        }
        if (isStarted()) {
            super.deliverResult(data);
        }
    }

    @Override
    protected void onStartLoading() {
        if (mRepository.cachedTransactionsAvailable()) {
            deliverResult(mRepository.getTransactions());
        }

        if (takeContentChanged() || !mRepository.cachedTransactionsAvailable()) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        onStopLoading();
    }
}
