package com.appviewer.appviewer;

/**
 * Created by Julya on 31.03.2017.
 */

public interface BasePresenter {

    void start();

}
