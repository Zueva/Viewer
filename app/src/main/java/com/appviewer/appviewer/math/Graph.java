package com.appviewer.appviewer.math;

import android.support.annotation.NonNull;

import com.appviewer.appviewer.other.Constants;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by Julya on 04.04.2017.
 */

public class Graph{
    Map<Vertex, List<Edge>> adj = new HashMap<>();
    private Map<Vertex, Boolean> used = new HashMap<>();
    private Map<Vertex, Vertex> paths = new HashMap<>();
    private Map<Vertex, BigDecimal> costs = new HashMap<>();
    private Map<Vertex, BigDecimal> reverseCosts = new HashMap<>();

    public void addEdge(Vertex vertex, Edge edge) {
        if (adj.containsKey(vertex)) {
            List<Edge> edges = adj.get(vertex);
                if (!edges.contains(edge)) {
                    edges.add(edge);
                }
            } else {
                List<Edge> edges = new ArrayList<>();
                edges.add(edge);
                adj.put(vertex, edges);
            }
    }

    public void execute(Vertex start) {
        Queue<Vertex> queueUnvisited  = new PriorityQueue<>();
        queueUnvisited.add(start);
        used.put(start, true);
        paths.put(start, new Vertex(Constants.FIRST_VERTEX));
        costs.put(start, new BigDecimal(Constants.RATE));
        reverseCosts.put(start, new BigDecimal(Constants.RATE));

        while (!queueUnvisited.isEmpty()) {
            Vertex current = queueUnvisited.peek();
            queueUnvisited.poll();

            List<Edge> adjVertices = adj.get(current);
            for (int i = 0; i < adjVertices.size(); i++) {
                Vertex vertex = adjVertices.get(i).getDestination();
                BigDecimal weight = new BigDecimal(adjVertices.get(i).getWeight());
                if(!used.containsKey(vertex)) {
                    used.put(vertex, true);
                    queueUnvisited.add(vertex);
                    paths.put(vertex, current);
                    costs.put(vertex, weight);
                }
                else {
                    if(vertex.equals(paths.get(current)))
                        reverseCosts.put(current, weight);
                }
            }
        }
    }

    public BigDecimal getShortPath(Vertex vertex) {
        BigDecimal multipleCoef = new BigDecimal(Constants.RATE);
        while(!vertex.getName().equals(Constants.FIRST_VERTEX)) {
            BigDecimal reverseCost = reverseCosts.get(vertex);
            multipleCoef = multipleCoef.multiply(reverseCost);
            vertex = paths.get(vertex);
        }
        return multipleCoef;
    }

    public static class Vertex implements Comparable<Vertex>{
        private final String name;

        public Vertex(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public int hashCode() {
            return name.hashCode();
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (!(o instanceof Vertex)) {
                return false;
            }
            Vertex other = (Vertex) o;
            return  other.getName().equals(name);
        }

        @Override
        public int compareTo(@NonNull Vertex o) {
            if (o == null)
                throw new NullPointerException("Vertex must be non-NULL.");

            if (this.name.equals(o.getName()))
                return 0;
            else return -1;
        }
    }

    public static class Edge  {
        private final Vertex source;
        private final Vertex destination;
        private final String weight;

        public Edge(Vertex source, Vertex destination, String weight) {
            this.source = source;
            this.destination = destination;
            this.weight = weight;
        }

        public Vertex getDestination() {
            return destination;
        }

        public Vertex getSource() {
            return source;
        }
        public String getWeight() {
            return weight;
        }

        @Override
        public String toString() {
            return source + " " + destination;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 17;
            result = prime * result + source.hashCode();
            result = prime * result + destination.hashCode();
            result = prime * result + weight.hashCode();
            return result;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (!(o instanceof Edge)) {
                return false;
            }

            Edge other = (Edge) o;

            return  other.getSource().equals(source) &&
                    other.getDestination().equals(destination) &&
                    other.getWeight() == weight;
        }
    }
}
