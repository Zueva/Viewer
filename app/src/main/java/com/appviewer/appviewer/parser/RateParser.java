package com.appviewer.appviewer.parser;

import android.util.JsonReader;

import com.appviewer.appviewer.entity.Rate;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yulya on 24.03.2017.
 */

public class RateParser {
    public List<Rate> readJsonStream(InputStream in) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
        try {
            return readRatesArray(reader);
        } finally {
            reader.close();
        }
    }

    private List<Rate> readRatesArray(JsonReader reader) throws IOException {
        List<Rate> rates = getRates(reader);
        return rates;
    }

    private List<Rate> getRates(JsonReader reader) throws IOException {
        List<Rate> rates = new ArrayList<>();
        String from = null;
        String rate = null;
        String to = null;

        reader.beginArray();
        while (reader.hasNext()) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("from")) {
                    from = reader.nextString();
                } else if (name.equals("rate")) {
                    rate = reader.nextString();
                } else if (name.equals("to")) {
                    to = reader.nextString();
                }
            }
            reader.endObject();
            rates.add(new Rate(from, rate, to));
        }
        reader.endArray();
        return rates;
    }
}
