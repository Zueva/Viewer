package com.appviewer.appviewer.transactions;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.appviewer.appviewer.R;
import com.appviewer.appviewer.entity.Product;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yulya on 24.03.2017.
 */

public class TransactionAdapter extends BaseAdapter{
    private List<Product> transactions = new ArrayList();
    private Context mContext;

    public TransactionAdapter(Context context) {
        mContext = context;
    }

    public void setTransactions(List<Product> transactions) {
        this.transactions = transactions;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return transactions.size();
    }

    @Override
    public Object getItem(int position) {
        return transactions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;
        Product product = (Product) getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_transaction, null);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)view.getTag();
        }
        viewHolder.amount.setText(product.getAmount());
        viewHolder.currency.setText(product.getCurrency());
        viewHolder.ramount.setText(String.format("£%s", product.getAmountConverted()));
        return view;
    }

    class ViewHolder {
        View view;
        @BindView(R.id.tvAmount)
        TextView amount;
        @BindView(R.id.tvCurrency)
        TextView currency;
        @BindView(R.id.tvResultAmount)
        TextView ramount;
        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }
}
