package com.appviewer.appviewer.transactions;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.*;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.appviewer.appviewer.R;
import com.appviewer.appviewer.ViewerApplication;
import com.appviewer.appviewer.entity.Product;
import com.appviewer.appviewer.entity.Transactions;
import com.appviewer.appviewer.other.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yulya on 24.03.2017.
 */

public class TransactionActivity extends AppCompatActivity implements TransactionContract.View {
    @Inject
    TransactionsPresenter mTransactionPresenter;

    @BindView(R.id.lv)
    protected ListView lvTransactions;
    @BindView(R.id.tvTotal)
    protected TextView tvTotal;
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    private TransactionAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_product);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        String name = intent.getStringExtra(Constants.PRODUCT_NAME);

        toolbar.setTitle(String.format("Transactions for %s", name));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ViewerApplication.getComponent().inject(this);
        mTransactionPresenter.setView(this);

        mAdapter = new TransactionAdapter(this);
        lvTransactions.setAdapter(mAdapter);
        mTransactionPresenter.getTransactions(name);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showTransactions(Transactions transactions) {
        tvTotal.setText(String.format("Total: £%s", transactions.getTotal()));
        tvTotal.setVisibility(View.VISIBLE);

        List<Product> products = new ArrayList<>();
        products.addAll(transactions.getTransactions());
        mAdapter.setTransactions(products);
    }

    @Override
    public void showNoTransactions() {

        Toast.makeText(this, getString(R.string.empty_data), Toast.LENGTH_SHORT).show();
    }
}
