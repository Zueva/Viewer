package com.appviewer.appviewer.transactions;

import com.appviewer.appviewer.entity.Transactions;

/**
 * Created by Julya on 10.04.2017.
 */

public interface TransactionContract {

    interface View {

        void showTransactions(Transactions transactions);

        void showNoTransactions();
    }

    interface Presenter {

        void getTransactions(String name);
    }
}
