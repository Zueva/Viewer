package com.appviewer.appviewer.transactions;

import com.appviewer.appviewer.entity.Transactions;
import com.appviewer.appviewer.repository.ProductsRepository;

import java.util.Map;

import javax.inject.Inject;

/**
 * Created by Julya on 10.04.2017.
 */

public class TransactionsPresenter implements TransactionContract.Presenter{

    private final ProductsRepository mProductsRepository;
    TransactionContract.View mTransactionView;

    @Inject
    public TransactionsPresenter(ProductsRepository mProductsRepository) {
        this.mProductsRepository = mProductsRepository;
    }

    @Override
    public void getTransactions(String name) {
        if (mProductsRepository.cachedTransactionsAvailable()) {
            Map<String, Transactions> products = mProductsRepository.getTransactions();
            Transactions transactions = products.get(name);
            if (transactions != null) {
                mTransactionView.showTransactions(transactions);
            }
        }else mTransactionView.showNoTransactions();
    }

    public void setView(TransactionContract.View view) {
        mTransactionView = view;
    }
}
