package com.appviewer.appviewer.repository;

import android.support.annotation.NonNull;

import com.appviewer.appviewer.entity.Product;
import com.appviewer.appviewer.entity.Rate;
import com.appviewer.appviewer.entity.Transactions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Julya on 31.03.2017.
 */
@Singleton
public class ProductsRepository implements DataStore {

    private final DataStore mLocalDataStore;

    private final DataStore mRemoteDataStore;

    List<Product> mCachedProducts;
    List<Rate> mCachedRates;
    Map<String, Transactions> mCache;

    boolean mCacheProductsIsDirty;
    boolean mCacheRatesIsDirty;


    @Inject
    public ProductsRepository(LocalDataStore mLocalDataStore, RemoteDataStore mRemoteDataStore) {
        this.mLocalDataStore = mLocalDataStore;
        this.mRemoteDataStore = mRemoteDataStore;
    }

    @Override
    public List<Product> getProducts() {
        List<Product> products = null;

        if (!mCacheProductsIsDirty) {
            if(mCachedProducts != null) {
                return getCachedProducts();
            } else {
                products = mLocalDataStore.getProducts();
            }
        }

        if (products == null || products.isEmpty()) {
            products = mRemoteDataStore.getProducts();
            saveProductsInLocalDataSource(products);
        }

        processLoadedProducts(products);
        return getCachedProducts();
    }
    @Override
    public List<Rate> getRates() {
        List<Rate> rates = null;
        if (!mCacheRatesIsDirty) {
            if(mCachedRates != null) {
                return getCachedRates();
            } else {
                rates = mLocalDataStore.getRates();
            }
        }

        if (rates == null || rates.isEmpty()) {
            rates = mRemoteDataStore.getRates();
            saveRatesInLocalDataSource(rates);
        }

        processLoadedRates(rates);
        return getCachedRates();
    }

    public List<Rate> getCachedRates() {
        return mCachedRates;
    }

    private void saveRatesInLocalDataSource(List<Rate> rates) {
        if (rates != null) {
            for(Rate rate : rates) {
                mLocalDataStore.saveRate(rate);
            }
        }
    }

    private void saveProductsInLocalDataSource(List<Product> products) {
        if (products != null) {
            for(Product product : products) {
                mLocalDataStore.saveProduct(product);
            }
        }
    }

    private void processLoadedRates(List<Rate> rates) {
        if (rates == null) {
            mCachedRates = null;
            mCacheRatesIsDirty = false;
            return;
        }

        if (mCachedRates == null) {
            mCachedRates = new ArrayList<>();
        }

        mCachedRates.clear();
        mCachedRates.addAll(rates);
        mCacheRatesIsDirty = false;
    }

    private void processLoadedProducts(List<Product> products) {
        if (products == null) {
            mCachedProducts = null;
            mCacheProductsIsDirty = false;
            return;
        }

        if (mCachedProducts == null) {
            mCachedProducts = new ArrayList<>();
        }

        mCachedProducts.clear();
        mCachedProducts.addAll(products);
        mCacheProductsIsDirty = false;
    }

    public List<Product> getCachedProducts() {
        return mCachedProducts;
    }

    public boolean cachedProductsAvailable() {
        return mCachedProducts != null && !mCacheProductsIsDirty;
    }

    public boolean cachedTransactionsAvailable(){
        return mCache != null && !mCache.isEmpty();
    }

    @Override
    public void saveProduct(@NonNull Product product) {
        mLocalDataStore.saveProduct(product);

        if (mCachedProducts == null) {
            mCachedProducts = new ArrayList<>();
        }

        mCachedProducts.add(product);
    }

    @Override
    public void saveRate(@NonNull Rate rate) {
        mLocalDataStore.saveRate(rate);

        if (mCachedRates == null) {
            mCachedRates = new ArrayList<>();
        }

        mCachedRates.add(rate);
    }

    @Override
    public void saveTransactions(Map<String, Transactions> cache) {
        if (mCache == null) {
            mCache = new HashMap<>();
        }
        mCache.putAll(cache);
    }

    @Override
    public Map<String, Transactions> getTransactions() {
        return mCache;
    }

    public interface LoadDataCallback {
        void onDataLoaded(Map<String, Transactions> data);

        void onDataEmpty();

        void onDataNotAvailable();

        void onDataReset();
    }
}
