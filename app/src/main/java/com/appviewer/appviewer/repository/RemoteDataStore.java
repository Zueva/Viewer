package com.appviewer.appviewer.repository;

import android.support.annotation.NonNull;

import com.appviewer.appviewer.entity.Product;
import com.appviewer.appviewer.entity.Rate;
import com.appviewer.appviewer.entity.Transactions;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by Julya on 31.03.2017.
 */

public class RemoteDataStore implements DataStore {
    @Inject
    public RemoteDataStore() {}

    @Override
    public List<Product> getProducts() {
        return null;
    }

    @Override
    public List<Rate> getRates() {
        return null;
    }

    @Override
    public void saveProduct(@NonNull Product product) {
    }

    @Override
    public void saveRate(@NonNull Rate rate) {
    }

    @Override
    public void saveTransactions(Map<String, Transactions> cache) {
    }

    @Override
    public Map<String, Transactions> getTransactions() {
        return null;
    }
}
