package com.appviewer.appviewer.repository;


import android.support.annotation.NonNull;

import com.appviewer.appviewer.entity.Product;
import com.appviewer.appviewer.entity.Rate;
import com.appviewer.appviewer.entity.Transactions;
import com.appviewer.appviewer.other.Constants;
import com.appviewer.appviewer.resources.DataLoader;
import com.appviewer.appviewer.resources.Loader;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by Yulya on 24.03.2017.
 */

public class LocalDataStore implements DataStore {
    private final Loader loader;

    @Inject
    public LocalDataStore(DataLoader loader) {
        this.loader = loader;
    }

    @Override
    public List<Product> getProducts() {
        if (loader.isExist(Constants.TRANSACTIONS)) return loader.getProducts();
        return null;
    }

    @Override
    public List<Rate> getRates() {
        if (loader.isExist(Constants.RATES)) return loader.getRates();
        return null;
    }

    @Override
    public void saveProduct(@NonNull Product product) {}

    @Override
    public void saveRate(@NonNull Rate rate) {
    }

    @Override
    public void saveTransactions(Map<String, Transactions> cache) {
    }

    @Override
    public Map<String, Transactions> getTransactions() {
        return null;
    }
}
