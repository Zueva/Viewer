package com.appviewer.appviewer.repository;


import android.support.annotation.NonNull;

import com.appviewer.appviewer.entity.Product;
import com.appviewer.appviewer.entity.Rate;
import com.appviewer.appviewer.entity.Transactions;

import java.util.List;
import java.util.Map;

/**
 * Created by Yulya on 24.03.2017.
 */

public interface DataStore {

    List<Product> getProducts();

    List<Rate> getRates();

    void saveProduct(@NonNull Product product);

    void saveRate(@NonNull Rate rate);

    void saveTransactions(Map<String, Transactions> cache);

    Map<String, Transactions> getTransactions();
}
