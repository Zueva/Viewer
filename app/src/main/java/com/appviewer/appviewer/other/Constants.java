package com.appviewer.appviewer.other;

/**
 * Created by Yulya on 24.03.2017.
 */

public interface Constants {
    String TRANSACTIONS = "transactions.json";

    String RATES = "rates.json";

    String PRODUCT_NAME = "content";

    String GBP = "GBP";

    String RATE = "1.0";

    String FIRST_VERTEX = "-1";
}
