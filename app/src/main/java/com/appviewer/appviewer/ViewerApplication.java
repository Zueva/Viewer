package com.appviewer.appviewer;

import android.app.Application;

import com.appviewer.appviewer.di.AppComponent;
import com.appviewer.appviewer.di.AppModule;
import com.appviewer.appviewer.di.DaggerAppComponent;

/**
 * Created by Yulya on 24.03.2017.
 */

public class ViewerApplication extends Application{
    private static AppComponent component;
    public static AppComponent getComponent() {
        return component;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        component = buildComponent();
    }

    protected AppComponent buildComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }
}
