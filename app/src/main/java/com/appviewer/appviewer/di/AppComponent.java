package com.appviewer.appviewer.di;

import com.appviewer.appviewer.products.ProductsActivity;
import com.appviewer.appviewer.products.ProductsPresenter;
import com.appviewer.appviewer.transactions.TransactionActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Yulya on 24.03.2017.
 */
@Singleton
@Component(modules = {AppModule.class, ViewModule.class})
public interface AppComponent {

    void inject(ProductsActivity activity);

    void inject(ProductsPresenter presenter);

    void inject(TransactionActivity activity);
}
